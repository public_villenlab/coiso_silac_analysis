# coiso_silac_analysis

Repository is created and maintained by Ian Smith (last updated on 12/27/2021).

Pre-print in preparation


## Introduction

The **coiso_silac_analysis** repository provides the community with all of the relevant code, input data files, and pdfs for manuscript figure generation.

For the quantification pipeline to analyze SILAC coisolation MS/MS data, go to:

**[coiso_silac GitLab](https://gitlab.com/public_villenlab/coiso_silac)**



## Coisoaltion of Peptide Pair Manuscript Code and Final Figures

The manuscript is composed of 5 final figures:

1. Coisolation method workflow (**[Figure 1](https://coiso-silac-analysis.netlify.app/figure1_coiso_wf_examplespectra)**)
2. Coisolation method identification performance (**[Figure 2](https://coiso-silac-analysis.netlify.app/figure2_coiso_identifications_coisopair_batch2021_mix2_billfdr)**)
3. Coiso E-value prioritizes scans for SILAC pair coisolation (**[Figure 3](https://coiso-silac-analysis.netlify.app/figure3_coisolationevalue_mix2_k1)**)
4. Coisolation Lys0:Lys8 MS/MS quantification performance (**[Figure 4](https://coiso-silac-analysis.netlify.app/figure4_silac_coiso_quantification_mix2_k1_calcmass_iqr)**)
5. Coisolation method applied to overlapping isotopic distributions (**[Figure 5](https://coiso-silac-analysis.netlify.app/figure5_k6k8_quants_mix2_k1_ms1deconv)**)


**CLICK FIGURE 1-5 for Knitted .Rmd Reports**

Relevant code for supplementary figures are within the above Knitted reports. 


## **coiso_silac_analysis** repository contents:

The repository is split into 3 folders:

The first is the data folder that is organized by K0K8 and K6K8 SILAC coisolation analysis. Within, the data folder is separated by each SILAC label mixture then MS/MS acquisition type. LR_DDA stands for the Fig 1a MS/MS schema data where a left and right wide coisolation scans and a narrow window scan were acquired on each precursor. LR designates left and right offset wide scans without the narrow window scans. K0:K8 LR_DDA data is for Fig2, K0:K8 is for Fig3-4, and K6:K8 LR was for Fig5. Only relevant files for R are stored here due to space constraints. All files from raw to results are available via PRIDE. Generally, contents for each experiment contain PSMs in .txt or .pin formats. Pin files ending in filename_K1.pin are the ones that are filtered via the pin filter available in the third subfolder based on filters explained in Methods. Scans that were filtered for in the resultant pin file are in a scans folder with the corresponding SILAC ratio folder designation named scans.txt. The filter_pin.exe was used to filter filename.pin to filename_K1.pin with the scans.txt in the same folder. filename_K1.pin files were used in the coiso_silac pipeline with corresponding .mzML files in pride and MS1 Dinosaur files when relevant. Quantification result files have .csv endings. Filenames go in ascending order starting at 10:1 L:H all the way to 1:10 L:H.

The second folder is figures which contains spectral annotation data for individual spectra, Rmd code for generation of figures, and all of the figures. Figures are initially generated in R and moved into Adobe Illustrator for aesthetic purposes. Pre- and Post-Illustrator PDFs are there with the Illustrator version being labeled with a filename_Final.pdf ending. Illustrator derived PDFs are what is used in the manuscript.

The third folder is for the filter_pin executable. To use a .pin file and a scans.txt file must be in the folder with the filter_pin.exe application. The scans.txt file is a tab deliminated file where each row consist of a MS/MS scan number you wish to retain in the resultant filtered pin file. Upon dragging the .pin file on top of the executable a new pin file will be generated filtered by scans.txt. This will have the ending filename.pin.new, which I rename to filename_K1.pin for coiso_silac python package analysis as an input. 
